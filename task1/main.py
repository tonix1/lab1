import sys

def count_words_in_string(input_string):
    words = input_string.split(" ")
    return len(words)

if __name__ == "__main__":
    input_string = ' '.join(sys.argv[1:])
    word_count = count_words_in_string(input_string)
    print(f"Количество слов в строке: {word_count}")
    print(input_string)