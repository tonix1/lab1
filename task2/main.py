import psycopg2
import time
import unittest

class TestDatabaseAccess(unittest.TestCase):
 
    def test_database_connection(self):
        time.sleep(20)
        conn = psycopg2.connect(
            dbname="root",
            user="root",
            password="root",
            host="172.19.0.2",
            port="5432"
        )
        self.assertIsNotNone(conn)
        conn.close()
 
    def test_query_execution(self):
        time.sleep(20)
        conn = psycopg2.connect(
            dbname="root",
            user="root",
            password="root",
            host="172.19.0.2",
            port="5432"
        )
        cur = conn.cursor()
        cur.execute("SELECT subject, exam_date FROM RecordBooks")
        rows = cur.fetchall()
        cur.close()
        conn.close()
        self.assertGreater(len(rows), 0)
 
if __name__ == '__main__':
    unittest.main()

    time.sleep(20)

    conn = psycopg2.connect(
        dbname="root",
        user="root",
        password="root",
        host="172.19.0.2",
        port="5432"
    )

    cur = conn.cursor()

    cur.execute("SELECT subject, exam_date FROM RecordBooks")
    rows = cur.fetchall()

    print("Предмет\t\tДата экзамена\n")
    for row in rows:
        print(f"{row[0]}\t{row[1]}\n")

    cur.close()
    conn.close()