CREATE TABLE Students (
student_id SERIAL PRIMARY KEY,
first_name VARCHAR(50),
last_name VARCHAR(50),
birth_date DATE,
record_book_id INT
);

CREATE TABLE RecordBooks (
record_book_id SERIAL PRIMARY KEY,
student_id INT,
subject VARCHAR(50),
exam_date DATE,
teacher_name VARCHAR(100),
FOREIGN KEY (student_id) REFERENCES Students(student_id)
);

INSERT INTO Students (first_name, last_name, birth_date, record_book_id)
VALUES
('Иван', 'Иванов', '1998-05-15', 1),
('Петр', 'Петров', '1997-10-22', 2),
('Мария', 'Сидорова', '1999-03-30', 3),
('Елена', 'Козлова', '1996-12-18', 4),
('Алексей', 'Смирнов', '1997-08-07', 5);

INSERT INTO RecordBooks (student_id, subject, exam_date, teacher_name)
VALUES
(1, 'Физика', '2022-01-25', 'Соколов К.В.'),
(1, 'История', '2022-01-22', 'Иванов Д.А.'),
(1, 'Биология', '2022-01-19', 'Петрова О.Н.'),
(1, 'География', '2022-01-21', 'Козлова А.П.'),
(2, 'Математика', '2022-01-17', 'Сидоров Г.М.'),
(2, 'Физика', '2022-02-01', 'Иванова Н.П.'),
(2, 'История', '2022-01-30', 'Петров А.И.'),
(2, 'Литература', '2022-01-28', 'Сидоров М.В.'),
(3, 'Химия', '2022-01-24', 'Козлов И.Г.'),
(3, 'Математика', '2022-01-26', 'Смирнова Е.А.'),
(3, 'Физкультура', '2022-01-27', 'Кузнецов Л.П.'),
(3, 'Информатика', '2022-01-29', 'Александров С.С.'),
(4, 'Философия', '2022-01-23', 'Федоров Д.Д.'),
(4, 'Психология', '2022-02-02', 'Соловьев А.В.'),
(4, 'Иностранный язык', '2022-02-03', 'Тарасова Е.Е.'),
(4, 'Экономика', '2022-02-04', 'Никитина О.И.'),
(5, 'Инженерная графика', '2022-01-18', 'Королева Е.Р.'),
(5, 'Строительство', '2022-02-05', 'Соколова Т.И.'),
(5, 'Электротехника', '2022-01-30', 'Лебедев В.В.'),
(5, 'Программирование', '2022-01-31', 'Зайцев П.С.');